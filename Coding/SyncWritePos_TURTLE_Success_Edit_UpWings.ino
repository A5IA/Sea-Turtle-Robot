/*
异步写例子在SCS15中测试通过，如果测试其它型号SCS系列舵机请更改合适的位置、速度与延时参数。
*/

//Include the servo library in the program
#include <SCServo.h>

//Define a SCSCL, called 'sc'
SCSCL sc;

byte ID[4]; //Define ID ของเซอร์โวว่ามี4ตัว
int Speed;  //Define Speed
double pos1,pos2,pos3,pos4; //Define ตัวแปรรับค่า Feedback ตำแหน่งของเซอร์โว
double vol1,vol2,vol3,vol4; //Define ตัวแปรรับค่า Feedback ค่า Torque ของเซอร์โว

void setup()
{
  Serial.begin(1000000);  //กำหนด Serial ของ Servo นี้ ซึ่งจะมีค่า 1000000
  sc.pSerial = &Serial;   
  delay(10);

  //กำหนดเลขไอดี
  ID[0] = 1;
  ID[1] = 3;
  ID[2] = 2;
  ID[3] = 4;

  Speed = 300;  //กำหนด Speed
}

void loop()
{
  
  sc.WritePos(ID[0], 300, 0, Speed);  //สั่งมอเตอร์ตัวที่ 1 หมุนไปทิศ 300 ด้วยความเร็วที่กำหนด 
  pos1 = sc.ReadPos(1);   //รับค่า Feedback ตำแหน่งของ Servo ตัวที่ 1
  vol1 = sc.ReadLoad(1);  //รับค่า Feedback Torque ของ Servo ตัวที่ 1
  Serial.print("\n Servo1 position1:");   //Print ค่าตำแหน่งของ Servo ตัวที่ 1
  Serial.println(pos1, DEC);
  Serial.print("\n Servo1 Load1:");       //Print ค่า Torque ของ Servo ตัวที่ 1
  Serial.println(vol1, DEC);

  sc.WritePos(ID[1], 300, 0, Speed);  //สั่งมอเตอร์ตัวที่ 3 หมุนไปทิศ 300 ด้วยความเร็วที่กำหนด
  pos3 = sc.ReadPos(3);
  vol3 = sc.ReadLoad(3);
  Serial.print("\n Servo3 position3:");
  Serial.println(pos3, DEC);
  Serial.print("\n Servo3 Load3:");
  Serial.println(vol3, DEC);
  delay(667); //Delay กำหนดจาก [(P1-P0)/V]*1000+100

  sc.WritePos(ID[0], 210, 0, Speed);  //สั่งมอเตอร์ตัวที่ 1 หมุนไปทิศ 210 ด้วยความเร็วที่กำหนด
  pos1 = sc.ReadPos(1);
  vol1 = sc.ReadLoad(1);
  Serial.print("\n Servo1 position1:");
  Serial.println(pos1, DEC);
  Serial.print("\n Servo1 Load1:");
  Serial.println(vol1, DEC);

  sc.WritePos(ID[1], 210, 0, Speed);  //สั่งมอเตอร์ตัวที่ 3 หมุนไปทิศ 210 ด้วยความเร็วที่กำหนด
  pos3 = sc.ReadPos(3);
  vol3 = sc.ReadLoad(3);
  Serial.print("\n Servo3 position3:");
  Serial.println(pos3, DEC);
  Serial.print("\n Servo3 Load3:");
  Serial.println(vol3, DEC);

  sc.WritePos(ID[2], 402, 0, Speed);  //สั่งมอเตอร์ตัวที่ 2 หมุนไปทิศ 402 ด้วยความเร็วที่กำหนด
  pos2 = sc.ReadPos(2);
  vol2 = sc.ReadLoad(2);
  Serial.print("\n Servo2 position2:");
  Serial.println(pos2, DEC);
  Serial.print("\n Servo2 Load2:");
  Serial.println(vol2, DEC);

  sc.WritePos(ID[3], 314, 0, Speed);  //สั่งมอเตอร์ตัวที่ 4 หมุนไปทิศ 314 ด้วยความเร็วที่กำหนด
  pos4 = sc.ReadPos(4);
  vol4 = sc.ReadLoad(4);
  Serial.print("\n Servo4 position4:");
  Serial.println(pos4, DEC);
  Serial.print("\n Servo4 Load4:");
  Serial.println(vol4, DEC);
  delay(744); //Delay กำหนดจาก [(P1-P0)/V]*1000+100

  sc.WritePos(ID[0], 510, 0, Speed);  //สั่งมอเตอร์ตัวที่ 1 หมุนไปทิศ 510 ด้วยความเร็วที่กำหนด
  pos1 = sc.ReadPos(1);
  vol1 = sc.ReadLoad(1);
  Serial.print("\n Servo1 position1:");
  Serial.println(pos1, DEC);
  Serial.print("\n Servo1 Load1:");
  Serial.println(vol1, DEC);

  sc.WritePos(ID[1], 510, 0, Speed);  //สั่งมอเตอร์ตัวที่ 3 หมุนไปทิศ 510 ด้วยความเร็วที่กำหนด
  pos3 = sc.ReadPos(3);
  vol3 = sc.ReadLoad(3);
  Serial.print("\n Servo3 position3:");
  Serial.println(pos3, DEC);
  Serial.print("\n Servo3 Load3:");
  Serial.println(vol3, DEC);
  delay(1000);  //Delay กำหนดจาก [(P1-P0)/V]*1000+100

  sc.WritePos(ID[2], 149, 0, 125);  //สั่งมอเตอร์ตัวที่ 2 หมุนไปทิศ 149 ด้วยความเร็ว 125
  pos2 = sc.ReadPos(2);
  vol2 = sc.ReadLoad(2);
  Serial.print("\n Servo2 position2:");
  Serial.println(pos2, DEC);
  Serial.print("\n Servo2 Load2:");
  Serial.println(vol2, DEC);

  sc.WritePos(ID[3], 61, 0, 125); //สั่งมอเตอร์ตัวที่ 4 หมุนไปทิศ 61 ด้วยความเร็ว 125
  pos4 = sc.ReadPos(4);
  vol4 = sc.ReadLoad(4);
  Serial.print("\n Servo4 position4:");
  Serial.println(pos4, DEC);
  Serial.print("\n Servo4 Load4:");
  Serial.println(vol4, DEC);
  delay(2124);   //Delay กำหนดจาก [(P1-P0)/V]*1000+100
}
